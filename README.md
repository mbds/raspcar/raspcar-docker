# Dockerisation du projet

## Groupe de TPT 1 - RaspCar

  •	Bah Mamadou Saliou
  
  •	Chatti Nader
  
  •	Diaz Gabriel
  
  •	Iben Salah Roukaya
  
  •	Ramoul Inès
  

## contenu :

Ce repo contient les images des développements réalisées à savoir :

  • Le Dashboard web

  • Le serveur web

## Recupération des images
Cloner le dépot
```sh
$ git clone https://gitlab.com/mbds/raspcar/raspcar-docker.git raspcar_docker
$ cd raspcar_docker
```
  Dézipper les fichiers
```sh
$ gunzip node_web_service.tar.gz react_dashboard.tar.gz
```

Charger les images docker
```sh
$ docker load < node_web_service.tar
$ docker load < react_dashboard.tar
```

Retrouver les images
```sh
$ docker images
```

Exécuter les images
```sh
$ docker run -p 8082:8082 -d raspcar/node_web_service
$ docker run -p 3000:3000 -d raspcar/react_dashboard
```